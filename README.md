# PHP Simple Crawler Checker

## Description
description.

## Requires
* PHP 5.3.0 or Higher

## Installation

include for `yicr/php-simple-crawler-checker` in your `composer.json` file. For example:

```bash
$ composer.phar require yicr/php-simple-crawler-checker
```

```json
{
    "require": {
        "yicr/php-simple-crawler-checker": "*"
    }
}
```

## Example

example

```php

require __DIR__ . '/vendor/autoload.php';


use Yicr\SimpleCrawlerChecker\Checker;

$crawlers = include('config/crawlers.php');

$checker = new Checker($crawlers);
if ($checker->check('ua')) {
    echo 'Yes!';
} else {
    echo 'No';
}

```

Thx.
