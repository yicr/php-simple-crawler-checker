<?php
/**
 * Sample
 */

require_once 'vendor/autoload.php';

use Yicr\SimpleCrawlerChecker\Checker;

$crawlers = include('config/crawlers.php');

$checker = new Checker($crawlers);
if ($checker->check('ua')) {
    echo 'is crawler' . PHP_EOL;
} else {
    echo 'is not crawler' . PHP_EOL;
}
