<?php
namespace Yicr\SimpleCrawlerChecker\Tests {

    use Yicr\SimpleCrawlerChecker\Checker;

    /**
     * Class ClientTest
     * @package     Yicr\SimpleJsonApiClient\Test
     * @access      public
     * @property    Checker $checker
     */
    class ClientTest extends \PHPUnit_Framework_TestCase
    {

        /**
         * setUp method
         *
         * @return void
         */
        public function setUp()
        {
            parent::setUp();
            $this->checker = new Checker();
        }

        /**
         * tearDown method
         *
         * @return void
         */
        public function tearDown()
        {
            unset($this->Checker);
            parent::tearDown();
        }

        public function testCheck()
        {
            // Internet Explorer
            $this->assertFalse($this->checker->check('Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; YTB730)'));
            $this->assertFalse($this->checker->check('Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; YTB730; GTB7.3; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30618; .NET4.0C)'));
            // Firefox
            $this->assertFalse($this->checker->check('Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0'));
            $this->assertFalse($this->checker->check('Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0'));
            // Safari
            $this->assertFalse($this->checker->check('Mozilla/5.0 (Macintosh; U; Intel Mac OS X; ja-jp) AppleWebKit/523.12 (KHTML, like Gecko) Version/3.0.4 Safari/523.12'));
            $this->assertFalse($this->checker->check('Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_4_11; ja-jp) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/4.1.3 Safari/533.19.4'));
            // Chrome
            $this->assertFalse($this->checker->check('Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.79 Safari/535.11'));
            $this->assertFalse($this->checker->check('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2'));
            // Opera
            $this->assertFalse($this->checker->check('Opera/9.80 (Windows NT 6.1; U; ja) Presto/2.10.229 Version/11.60'));
            $this->assertFalse($this->checker->check('Opera/9.80 (Windows NT 6.0; U; ja) Presto/2.5.22 Version/10.51'));
            // Android
            $this->assertFalse($this->checker->check('Mozilla/5.0 (Linux; U; Android 3.1; ja-jp; Sony Tablet S Build/THMAS10000) AppleWebKit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13'));
            $this->assertFalse($this->checker->check('Mozilla/5.0 (Linux; U; Android 2.3.5; ja-jp; T-01D Build/V39R38A) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1'));
            // iPhone
            $this->assertFalse($this->checker->check('Mozilla/5.0 (iPhone; CPU iPhone OS 5_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B176 Safari/7534.48.3'));
            $this->assertFalse($this->checker->check('Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; ja-jp) AppleWebKit/533.17.9 (KHTML, like Gecko) Mobile/8J2'));
            // iPad
            $this->assertFalse($this->checker->check('Mozilla/5.0 (iPad; CPU OS 5_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B176 Safari/7534.48.3'));
            $this->assertFalse($this->checker->check('Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; ja-jp) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B405 Safari/531.21.10'));
            // Opera Mini
            $this->assertFalse($this->checker->check('Opera/9.80 (Android; Opera Mini/6.5.27452/27.1324; U; ja) Presto/2.8.119 Version/11.10'));
            // Bot
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)'));
            $this->assertTrue($this->checker->check('Yeti/1.0 (NHN Corp.; http://help.naver.com/robots/)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)'));
            $this->assertTrue($this->checker->check('DoCoMo/2.0 N905i(c100;TB;W24H16) (compatible; Googlebot-Mobile/2.1; +http://www.google.com/bot.html)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; Steeler/3.5; http://www.tkl.iis.u-tokyo.ac.jp/~crawler/)'));
            $this->assertTrue($this->checker->check('ichiro/3.0 (http://help.goo.ne.jp/door/crawler.html)'));
            $this->assertTrue($this->checker->check('hotpage.fr (http://www.hotpage.fr)'));
            $this->assertTrue($this->checker->check('Feedfetcher-Google; (+http://www.google.com/feedfetcher.html; 1 subscribers; feed-id=6741455251313593689)'));
            $this->assertTrue($this->checker->check('Baiduspider+(+http://www.baidu.com/search/spider.htm)'));
            $this->assertTrue($this->checker->check('livedoor FeedFetcher/0.01 (http://reader.livedoor.com/; 1 subscriber)'));
            $this->assertTrue($this->checker->check('ia_archiver (+http://www.alexa.com/site/help/webmasters; crawler@alexa.com)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; SISTRIX Crawler; http://crawler.sistrix.net/)'));
            $this->assertTrue($this->checker->check('msnbot-media/1.1 (+http://search.msn.com/msnbot.htm)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; zenback bot; powered by logly +http://www.logly.co.jp/)'));
            $this->assertTrue($this->checker->check('Y!J-BRI/0.0.1 crawler ( http://help.yahoo.co.jp/help/jp/search/indexing/indexing-15.html )'));
            $this->assertTrue($this->checker->check('TurnitinBot/2.1 (http://www.turnitin.com/robot/crawlerinfo.html)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; Google Desktop/5.9.1005.12335; http://desktop.google.com/)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; newzia crawler +http://www.logly.co.jp/)'));
            $this->assertTrue($this->checker->check('DoCoMo/2.0 P05A(c100;TB;W24H15) (compatible; BaiduMobaider/1.0; +http://www.baidu.jp/spider/)'));
            $this->assertTrue($this->checker->check('Y!J-BRJ/YATS crawler (http://listing.yahoo.co.jp/support/faq/int/other/other_001.html)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; Seznam screenshot-generator 2.0; +http://fulltext.sblog.cz/screenshot/)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; SiteBot/0.1; +http://www.sitebot.org/robot/)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; Purebot/1.1; +http://www.puritysearch.net/)'));
            $this->assertTrue($this->checker->check('emBot-GalaBuzz/Nutch-1.0 (http://emining.jp/; em@galabuzz.jp)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; Search17Bot/1.1; http://www.search17.com/bot.php)'));
            $this->assertTrue($this->checker->check('DoCoMo/2.0 P900i(c100;TB;W24H11) (compatible; ichiro/mobile goo; +http://help.goo.ne.jp/help/article/1142/)'));
            $this->assertTrue($this->checker->check('YPBot/Raven1.1.3 (compatible; Googlebot/2.1;+http://www.yellowpages.com/about/legal/crawl)'));
            $this->assertTrue($this->checker->check('Mozilla/4.0 (Toread-Crawler/1.1; +http://news.toread.cc/crawler.php)'));
            $this->assertTrue($this->checker->check('Tumblr/1.0 RSS syndication (+http://www.tumblr.com/) (support@tumblr.com)'));
            $this->assertTrue($this->checker->check('Mozilla/5.0 (compatible; DotBot/1.1; http://www.dotnetdotcom.org/, crawler@dotnetdotcom.org)'));
            $this->assertTrue($this->checker->check('Chilkat/1.0.0 (+http://www.chilkatsoft.com/ChilkatHttpUA.asp)'));
        }
    }
}
