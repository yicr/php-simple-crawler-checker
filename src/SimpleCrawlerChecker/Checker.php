<?php
namespace Yicr\SimpleCrawlerChecker {

    /**
     * Class Checker
     *
     * @package Yicr\SimpleCrawlerChecker
     */
    class Checker
    {
        /**
         * @var array default crawler list
         */
        protected $crawlers = array();

        /**
         * Checker constructor.
         *
         * @param array $include target crawler list.
         */
        public function __construct(array $crawlers)
        {
            $this->crawlers = $crawlers;
        }

        /**
         * Check crawler ua
         *
         * @param string $ua http user agent
         * @return bool check result
         */
        public function check($ua)
        {
            foreach ($this->crawlers as $item) {
                if (stripos($ua, $item) !== false) {
                    return true;
                }
            }
            return false;
        }
    }
}
